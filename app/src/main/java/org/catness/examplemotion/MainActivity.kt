package org.catness.examplemotion

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.utils.widget.ImageFilterView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import org.catness.examplemotion.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    // Contains all the views
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Use Data Binding to get reference to the views
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //setContentView(R.layout.activity_main)
        var layout: MotionLayout = binding.layout

        // create deep copies of the cat drawable for all the views
        // to avoid corrupting its state, otherwise the views use and change the same drawable
        var cat1: Drawable? = ContextCompat.getDrawable(applicationContext, R.drawable.cat1)
        binding.apply {
            var images: List<ImageFilterView> = listOf(image1, image2, image3, image4, image5, image6)
            images.forEach { it.setImageDrawable(cat1?.copy()) }
        }

        // to transition back and forth
        binding.button.setOnClickListener {
            //Log.i("MAIN", "onClick called")
            //Log.i("MAIN", "state = $layout.currentState.toString()")
            if (layout.currentState == R.id.start) {
                layout.transitionToEnd()
            } else {
                layout.transitionToStart()
            }
        }
    }

}
// extension: deep copy of a drawable
fun Drawable.copy() = constantState?.newDrawable()?.mutate()
