# About the project

**ExampleMotion** is an example for [Advanced Android in Kotlin 03.2: Animation with MotionLayout](https://codelabs.developers.google.com/codelabs/motion-layout). It only implements 3 features: a moving button, a scrolling parallax header, and a set of images cross-fading with different effects. All the animations are triggered by clicking the button.

Probably my way of using tiled images for the header by using different drawables is not the optimal, but I couldn't figure out another way of doing it. Every tile is colored differently for the convenience of debugging.


Cat images are from Pixabay:

* https://pixabay.com/photos/cat-domestic-cat-animal-pet-small-3690245/
* https://pixabay.com/photos/nature-cat-field-meadow-grass-957662/

Parallax assets are by Luis Zuno (@ansimuz) from https://pixelgameart.org, I don't have the exact link, I think I got it from some bundle.

The apk is here: [examplemotion.apk](apk/examplemotion.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
